# README #

Array Problems

### What is this repository for? ###

* Problem using array

### How do I set it up? ###

* Maven project

### Contribution guidelines ###

* Follow below package names 
* for java solution com.jiomitwa.ds.java.basic
* For scala solution com.jiomitwa.ds.scala.basic
* For each solution class write a JUnit with different unit test cases
* print time complexity in each solution with explanation

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Assignment ###
1. 	Find second largest no from given array of integer? -DONE
2. 	Find second minimum number? 
3. 	How to find the missing no from give array of integer which contain no from 1 to 100 -DONE {Done for numbers 1-5} -DONE
4. 	Find is given array sorted? -DONE
5. 	Find the 2 number which sum is maximum in given array of integer -DONE
6. 	Find out are given two array both contain all the same number -DONE
7. 	Given 2 Array of integer, find out that first array is subset of the second array -DONE
8. 	Find out that given array of character is Palindrome
9. 	Write a java class to make circular array
10. Reverse the character array
11. Write a recursive program to multiply all the array element
12. Write a java class for the big integer array
13. Parallel program to compare 2 sorted array
14. 2 array of integer are given, merge both array elements 
15. Remove duplicates from array in Java
17. Array of Character is given, write a program to calculate frequency of the elements, output should be in sorted order of frequency.
18. find the maximum distance in array of elements
19. write a java program to create dynamic array with add and delete functionality 
20. get the sub array of array of integer which sum is maximum
21. 2-D for to inverse matrix
22. Get the sub array of array of integer which sum is minimum
23. Find the mean and mod of the array
24. Array of character is given with length N, print all the words which can be formed from it.
25. Make Stack using array
26. Make Queue using array

27. Program for array rotation
28. Find largest and smallest number in unsorted array -DONE
29.	Find all pairs on integer array whose sum is equal to given number
15. Remove duplicates from array in Java
31. Find if there is a sub array inside an array with sum equal to zero
