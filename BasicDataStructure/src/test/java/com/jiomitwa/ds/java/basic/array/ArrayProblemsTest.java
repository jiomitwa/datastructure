package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.InvalidArugumentException;

import junit.framework.TestCase;

public class ArrayProblemsTest extends TestCase {

	/**
	 * Rigorous Test_SecondMax_inArray :-)
	 */
	public void test_SecondMax()
	{
		SecondMax_inArray ap = new SecondMax_inArray();
		try
		{
			int[] arr1 = { 1, 1, 1, 1 };
			assertEquals(5, ap.secondMax_sol1(arr1));
			assertEquals(5, ap.secondMax_sol2(arr1));

			int[] arr2 = { 1, 2, 3, 4, 5 };
			assertEquals(4, ap.secondMax_sol1(arr2));
			assertEquals(4, ap.secondMax_sol2(arr2));

		} catch (InvalidArugumentException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Rigorous Test_MaxsuminArray :-)
	 */
	public void test_MaxsuminArray()
	{
		Maximum_Sum_inArray maxsum = new Maximum_Sum_inArray();

		try
		{
			int[] arr1 = { 1, 1, 1, 1, 1 };
			assertEquals(2, maxsum.maxsuminarray_sol1(arr1));

			int[] arr2 = { 1, 2, 3, 4, 5 };
			assertEquals(9, maxsum.maxsuminarray_sol1(arr2));

		} catch (InvalidArugumentException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Rigorous Test_Arrayissorted :-)
	 */
	public void test_Arrayissorted()
	{
		int[] arr = { 2, 3, 4, 5 };
		SortedArray sorted = new SortedArray();
		try
		{
			assertEquals(true, sorted.arraysorted_sol1(arr));
			assertEquals(true, sorted.arraysorted_sol2(arr, arr.length));
		} catch (InvalidArugumentException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Rigorous Test_TwoArray_contain_sameelements :-)
	 * 
	 * @throws InvalidArugumentException
	 */
	public void test_TwoArray_contain_sameelements() throws InvalidArugumentException
	{
		int[] arrx = { 1, 2, 3, 4, 5 };
		int[] arry = { 1, 2, 3, 4, 5 };

		TwoArray_Contain_SameNumber contains = new TwoArray_Contain_SameNumber();
		contains.arrays_contain_samenumber_sol1(arrx, arry);
		contains.arrays_contain_samenumber_sol2(arrx, arry);
	}

	/**
	 * Rigorous Test_TwoArray_contain_sameelements :-)
	 * 
	 * @throws InvalidArugumentException
	 */
	public void test_Missing_no_inArray() throws InvalidArugumentException
	{
		int[] arr = { 1, 2, 4, 5 };
		Missing_number_inArray missingno = new Missing_number_inArray();
		missingno.find_missing_no_array_sol1(arr);
		missingno.find_missing_no_array_sol2(arr);
	}

	/**
	 * Rigorous Test_Largest_Smallest_unsortedArray :-)
	 * 
	 * @throws InvalidArugumentException
	 */
	public void test_largest_smallest_unsortedarray() throws InvalidArugumentException
	{
		int[] arr = { 7, 5, 3, 9, 1 };
		Largest_smallest_in_Array largestsmallest = new Largest_smallest_in_Array();
		largestsmallest.find_largest_smallest_inarray(arr);
	}

	/**
	 * Rigorous Test_Remove_duplicated_fromArray :-)
	 * 
	 * @throws InvalidArugumentException
	 * 
	 */
	public void test_remove_duplicates_inarray() throws InvalidArugumentException
	{
		int[] arr = { 1, 1, 1, 11 };
		RemoveDuplicates_Array removeduplicate = new RemoveDuplicates_Array();
		removeduplicate.remove_duplicates_inarray_sol1(arr);
	}

	/**
	 * Rigorous Test_First array subset of other :-)
	 */
	public void test_First_array_subset_ofother()
	{
		int[] arrx = { 1, 5, 5, 4, 2 };
		int[] arry = { 1, 3, 2, 4, 5 };

		ArraySubset_ofother arraysubset = new ArraySubset_ofother();
		arraysubset.array_subset_sol1(arrx, arry);
		arraysubset.array_subset_sol2(arrx, arry);
	}
}
