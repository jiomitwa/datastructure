package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class TwoArray_Contain_SameNumber {

	public String arrays_contain_samenumber_sol1(int[] arrx, int[] arry) throws InvalidArugumentException
	{
		if (arrx == null || arrx.length == 0 && arry == null || arry.length == 0)
			throw new InvalidArugumentException("One of the array is null or empty");

		boolean contains = false;
		// Check for every element of x in y
		mainx: for (int i = 0; i < arrx.length; i++)
		{
			contains = false;
			for (int j = 0; j < arry.length; j++)
			{
				if (arrx[i] == arry[j])
				{
					contains = true;
					continue mainx;
				}
			}
			if (!contains)
				break;
		}

		// Check for every element of y in x
		mainy: for (int i = 0; i < arry.length; i++)
		{
			contains = false;
			for (int j = 0; j < arrx.length; j++)
			{
				if (arry[i] == arrx[j])
				{
					contains = true;
					continue mainy;
				}
			}
			if (!contains)
				break;
		}

		DisplayTimeComplexity.displayTimeComplexity("2 Array contain exactly same elements", "O(n2)+O(n2)", "");
		return "2 Arrays contains same elements: " + contains;
	}

	// Using Binary Search for array
	// Conditions: Array is sorted
	public int arrays_contain_samenumber_sol2(int[] arrx, int[] arry) throws InvalidArugumentException
	{
		if (arrx == null || arrx.length == 0 && arry == null || arry.length == 0)
			throw new InvalidArugumentException("One of the array is null or empty");

		// Compare arrx with arry for same elements
		main: for (int i = 0; i < arrx.length; i++) // Time complexity - O(n)
		{
			int left = 0;
			int right = arry.length - 1;
			boolean missing = true;
			while (left <= right) // Time complexity - O(logn)
			{
				int middle = left + (right - left) / 2;

				if (arry[middle] == arrx[i])
				{
					missing = false;
					continue main;
				}

				else if (arry[middle] > arrx[i])
					right = middle - 1;

				else if (arry[middle] < arrx[i])
					left = middle + 1;
			}
			if (missing)
				return arrx[i];
		}

		// Compare arry with arrx for same element
		main: for (int j = 0; j < arry.length; j++) // Time complexity - O(n)
		{
			int left = 0;
			int right = arrx.length - 1;
			boolean missing = true;
			while (left <= right) // Time complexity - O(logn)
			{
				int middle = left + (right - left) / 2;

				if (arrx[middle] == arry[j])
				{
					missing = false;
					continue main;
				}

				else if (arrx[middle] > arry[j])
					right = middle - 1;

				else if (arrx[middle] < arry[j])
					left = middle + 1;

			}
			if (missing)
				return arry[j];
		}
		DisplayTimeComplexity.displayTimeComplexity("TwoArrayContainSameNumbers", "O(nlogn)+O(nlogn)", "");
		return -1;
	}
}
