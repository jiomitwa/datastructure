package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class Maximum_Sum_inArray {

	public int maxsuminarray_sol1(int[] arr) throws InvalidArugumentException
	{
		if (arr.length < 2)
			throw new InvalidArugumentException("Array has less than 2 elements");
		int max = arr[0];
		int smax = arr[1];

		for (int i = 0; i < arr.length; i++)
		{
			if (arr[i] >= smax)
			{
				if (arr[i] >= max)
				{
					smax = max;
					max = arr[i];
				}
				else
					smax = arr[i];
			}
		}
		DisplayTimeComplexity.displayTimeComplexity("Maximum sum in array", "O(n)+2", "with 2 variables");
		return max + smax;
	}
}
