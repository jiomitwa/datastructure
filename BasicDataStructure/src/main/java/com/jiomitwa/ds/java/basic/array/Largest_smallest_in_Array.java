package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class Largest_smallest_in_Array {

	public String find_largest_smallest_inarray(int[] x) throws InvalidArugumentException
	{
		if (x.length < 2)
			throw new InvalidArugumentException("Array length is less than 2");

		int smallest = x[0];
		int largest = x[1];

		for (int i = 0; i < x.length; i++)
		{
			if (x[i] < smallest)
				smallest = x[i];

			else if (x[i] > largest)
				largest = x[i];
		}
		DisplayTimeComplexity.displayTimeComplexity("Largest_smallest_unsortedarray", "O(n)", "Largest: " + largest + " " + "Smallest: " + smallest );
		return "Largest: " + largest + " " + "Smallest: " + smallest;
	}
}
