package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

// TODO: Auto-generated Javadoc
/**
 * The Class SecondMax_inArray.
 */
public class SecondMax_inArray {

	/*
	 * Problem 1 Given array of integer find second largest number
	 */
	public int secondMax_sol1(int[] arr) throws InvalidArugumentException
	{
		if (arr.length < 2)
			throw new InvalidArugumentException("Array is less than 2 elements");
		int max = 0;
		int sMax = 0;
		max = (arr[0] > arr[1]) ? arr[0] : arr[1];
		sMax = (arr[0] < arr[1]) ? arr[0] : arr[1];

		for (int i = 0; i < arr.length; i++)
		{
			if (arr[i] > sMax)
			{
				if (arr[i] > max)
				{
					sMax = max;
					max = arr[i];
				}
				else
				{
					sMax = arr[i];
				}
			}
		}
		DisplayTimeComplexity.displayTimeComplexity("Second Max", "O(n)+2", "with 2 variables");
		return sMax;
	}

	public int secondMax_sol2(int[] arr) throws InvalidArugumentException
	{
		if (arr.length < 2)
			throw new InvalidArugumentException("Array is less than 2 elements");
		int max = arr[0];
		int smax = 0;
		int start = 0;
		int end = arr.length;
		while (start < end)
		{
			if (arr[start] == max && start != end - 1)
				start++;

			else if (arr[start] > smax)
			{
				if (arr[start] > max)
				{
					smax = max;
					max = arr[start];
					start++;
				}
				else
				{
					smax = arr[start];
					start++;
				}
			}
			else
				start++;
		}
		System.out.println("Second Maximum_sol2: " + smax);
		DisplayTimeComplexity.displayTimeComplexity("Second Max", "O(n)+4", "with 4 variables");
		return smax;
	}
}
