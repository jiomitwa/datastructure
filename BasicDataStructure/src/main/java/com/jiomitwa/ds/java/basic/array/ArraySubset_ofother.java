package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;

public class ArraySubset_ofother {

	public String array_subset_sol1(int[] x, int[] y)
	{
		// Check if X is subset of Y
		boolean subset = false;
		main: for (int i = 0; i < x.length; i++) // Time Complexity -O(n)
		{
			subset = false;
			for (int j = 0; j < y.length; j++) // Time Complexity - O(n)
			{
				if (x[i] == y[j])
				{
					subset = true;
					continue main;
				}
			}
			if (!subset)
				break;
		}
		DisplayTimeComplexity.displayTimeComplexity("First Array subset of other", "O(n2)", String.valueOf(subset));
		return "Array x subset of Array y: " + subset;
	}

	// Using Binary Search
	// Conditions - Array is sorted
	public String array_subset_sol2(int[] arrx, int[] arry)
	{
		// Check if arrx is subset of arry
		mainx: for (int i = 0; i < arrx.length; i++) // Time Complexity - O(n)
		{
			boolean missing = true;
			int left = 0;
			int right = arry.length - 1;
			while (left <= right) // Time Complexity - O(logn)
			{
				int middle = left + (right - left) / 2;

				if (arry[middle] == arrx[i])
				{
					missing = false;
					continue mainx;
				}

				else if (arry[middle] > arrx[i])
					right = middle - 1;

				else if (arry[middle] < arrx[i])
					left = middle + 1;
			}
			if (missing)
			{
				DisplayTimeComplexity.displayTimeComplexity("Array subset of other", "O(nlogn)", String.valueOf(false));
				return "arrx subset of arry:" + false;
			}
		}
		DisplayTimeComplexity.displayTimeComplexity("Array subset of other", "O(nlogn)", String.valueOf(true));
		return "arrx subset of arry: " + true;
	}

}
