package com.jiomitwa.ds.java.basic.array;

import java.util.LinkedList;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class RemoveDuplicates_Array {

	public LinkedList<Integer> remove_duplicates_inarray_sol1(int[] arr) throws InvalidArugumentException
	{
		if (arr == null || arr.length == 0)
			throw new InvalidArugumentException("Array is null or empty");

		LinkedList<Integer> unique_list = new LinkedList<Integer>();

		for (int i = 0; i < arr.length; i++) // Time complexity - O(n)
		{
			if (i == arr.length - 1)
			{
				if (!unique_list.contains(arr[i]))
					unique_list.add(arr[i]);
				break;
			}

			boolean duplicatefound = false;

			if (arr[i] == arr[i + 1])
				duplicatefound = true;

			else if (!duplicatefound)
				unique_list.add(arr[i]);

		}
		DisplayTimeComplexity.displayTimeComplexity("Remove duplicates in array", "O(n)", String.valueOf(unique_list));
		return unique_list;
	}
}
