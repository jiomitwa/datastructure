package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class SortedArray {

	// Iteration for ascending order
	public boolean arraysorted_sol1(int[] arr) throws InvalidArugumentException
	{
		if (arr.length < 2)
			throw new InvalidArugumentException("Array size is less than 2");
		boolean sorted = false;

		for (int k = 0; k < arr.length; k++)
		{
			if (k == arr.length - 1)
				return sorted;

			else if (arr[k] <= arr[k + 1])
				sorted = true;

			else
			{
				sorted = false;
				break;
			}
		}
		DisplayTimeComplexity.displayTimeComplexity("Array is sorted", "O(n)+1", "with 1 variable");
		return sorted;
	}

	// recursion for ascending order
	public boolean arraysorted_sol2(int[] arr, int size)
	{
		if (size == 1 || arr == null)
			return true;

		if (arr[size - 1] < arr[size - 2])
			return false;

		return arraysorted_sol2(arr, size - 1);
	}
}
