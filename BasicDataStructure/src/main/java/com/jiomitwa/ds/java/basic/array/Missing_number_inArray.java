package com.jiomitwa.ds.java.basic.array;

import com.jiomitwa.ds.java.basic.DisplayTimeComplexity;
import com.jiomitwa.ds.java.basic.InvalidArugumentException;

public class Missing_number_inArray {

	// Missing no
	// Conditions: Comparison is for numbers from 1-5 , Array is sorted
	public String find_missing_no_array_sol1(int[] arr) throws InvalidArugumentException
	{
		if (arr == null || arr.length == 0)
			throw new InvalidArugumentException("Array is null or empty");

		int missing_no = 0;
		boolean missing = true;

		main: for (int i = 1; i <= 5; i++) // Time complexity - O(n)
		{
			missing = true;
			for (int j = 0; j < arr.length; j++) // Time complexity - O(n)
			{
				if (i == arr[j])
				{
					missing = false;
					continue main;
				}
				else
					missing_no = i;
			}
			if (missing == true)
				break;
		}

		DisplayTimeComplexity.displayTimeComplexity("Missing no in array", "O(n2)", "");
		System.out.println(missing_no);
		return "Missing no in array is: " + missing_no;
	}

	// Using Binary search for array
	// Conditions: Comparison is for numbers from 1-5 , Array is sorted
	public int find_missing_no_array_sol2(int[] arr) throws InvalidArugumentException
	{
		if (arr == null || arr.length == 0)
			throw new InvalidArugumentException("Array is null or empty");

		main: for (int i = 1; i <= 5; i++) // Time complexity - O(n)
		{
			int left = 0;
			int right = arr.length - 1;
			boolean missing = true;
			while (left <= right) // Time complexity - O(logn)
			{
				int middle = left + (right - left) / 2;

				if (arr[middle] == i)
				{
					missing = false;
					continue main;
				}

				else if (arr[middle] > i)
					right = middle - 1;

				else if (arr[middle] < i)
					left = middle + 1;
			}
			if (missing)
			{
				DisplayTimeComplexity.displayTimeComplexity("Missing_number in Array", "O(nlogn)", String.valueOf(i));
				return i;
			}
		}
		DisplayTimeComplexity.displayTimeComplexity("Missing_number in Array", "O(nlogn)", String.valueOf(-1));
		return -1;
	}
}
